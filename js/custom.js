/**
 * @file
 * Contains the clipboard js.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.commerce_user_point = {
    attach: function (context, settings) {

      // Referral point copy link.
      var sender = jQuery('#edit-sender-referral-points ').val();
      var user = jQuery('#new-user-referral-points ').val();
      $('.referral-link-section #copy-referral-link').click(function () {
        var link = $('.referral-link-section #user-referral-link').text();
        link = $.trim(link);
        navigator.clipboard.writeText(link);
        $('.referral-link-section #copy-referral-link').text('Copied!');

        // User Point checkbox
        jQuery(".user-point-checkbox").click(function () {
          if(jQuery(this).is(":checked")) {
             jQuery('.form-item-coupons-user-points-redemption').hide();
          } else {
            jQuery('.form-item-coupons-user-points-redemption').hide();
          }
        });
      });
      jQuery('.form-type-vertical-tabs').hide();
       jQuery(".role-configuration").click(function () {
          if(jQuery(this).is(":checked")) {
            jQuery('.form-item-new-user-referral-points').hide();
            jQuery('.form-item-sender-referral-points').hide();
            jQuery('.form-type-vertical-tabs').show();

            jQuery("#edit-sender-referral-points").val("");
            jQuery("#new-user-referral-points").val("");
          } else {
            jQuery('.form-item-new-user-referral-points').show();
            jQuery('.form-item-sender-referral-points').show();
            jQuery('.form-type-vertical-tabs').hide();
            jQuery('#edit-sender-referral-points').attr('value', sender);
            jQuery('#new-user-referral-points').attr('value', user);

            jQuery(".sender-referral-role").val("");
            jQuery(".new-user-referral-role").val("");
          }
        });

       jQuery(".role-configuration-threshold").click(function () {
          if(jQuery(this).is(":checked")) {
            jQuery('.form-item-user-threshold-amount').hide();
            jQuery('.form-type-vertical-tabs').show();

          } else {
            jQuery('.form-item-user-threshold-amount').show();
            jQuery('.form-type-vertical-tabs').hide();
            jQuery(".role-threshold-field").val("");

          }
        });

       jQuery(".role-configuration-user-point").click(function () {
          if(jQuery(this).is(":checked")) {
            jQuery('.form-item-user-register-points').hide();
            jQuery('.form-item-order-point-discount').hide();
            jQuery('.advance-settings').hide();
            jQuery('.form-type-vertical-tabs').show();

            jQuery("#edit-user-register-points").val("");
            jQuery("#edit-order-point-discount").val("");
            jQuery("#edit-day-point-discount").val("1");
            jQuery("#edit-date-point-discount").val("");
            jQuery("#edit-threshold-value").val("");
          } else {
             jQuery('.form-item-user-register-points').show();
             jQuery('.form-item-order-point-discount').show();
             jQuery('.advance-settings').show();
             jQuery('.form-type-vertical-tabs').hide();

             jQuery(".user-register-points-role").val("");
             jQuery(".order-point-role").val("");
             jQuery(".advance-settings-role").val("");
             jQuery(".date-discount-role").val("1");
             jQuery(".date-point-role").val("");
             jQuery(".threshold-points-role").val("");
          }
        });
    }
  }
})(jQuery, Drupal, drupalSettings);
