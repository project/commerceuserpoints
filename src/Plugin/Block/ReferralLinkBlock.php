<?php

namespace Drupal\commerce_user_points\Plugin\Block;

use Drupal\commerce_user_points\ReferralLinkService;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Referral Link' block.
 *
 * @Block(
 *   id = "user_referral_link",
 *   admin_label = @Translation("Referral Link")
 * )
 */
class ReferralLinkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The referral link service.
   *
   * @var \Drupal\commerce_user_points\ReferralLinkService
   */
  protected $referralLinkService;

  /**
   * Constructs a new YourCustomBlock object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\commerce_user_points\ReferralLinkService $referral_link_service
   *   The referral link service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,AccountProxyInterface $current_user, ReferralLinkService $referral_link_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->referralLinkService = $referral_link_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('commerce_user_points.referral_link')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $referralLink = "";
    $uid = $this->currentUser->id();
    if ($uid != NULL) {
      $referralLinkCode = $this->referralLinkService->getLink('uid', $uid);

      if (isset($referralLinkCode) && !empty($referralLinkCode)) {
        $linkCode = $referralLinkCode['referral_link_code'];
        $frontPage = Url::fromRoute('<front>')->setAbsolute()->toString();
        $referralLink = $frontPage . '/user/register/referral/' . $linkCode;
      }
    }

    $renderable = [
      '#theme' => 'user_referral_link',
      '#referral_link' => $referralLink,
      '#attached' => [
        'library' => [
          'commerce_user_points/referral_link_library',
        ],
      ],
    ];
    return $renderable;
  }

}
