<?php

namespace Drupal\commerce_user_points\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures forms module settings.
 */
class UserPointsConfigurationForm extends ConfigFormBase {

  /**
   * The role manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleManager;

  /**
   * Constructs a new YourConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->roleManager = $entity_type_manager->getStorage('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Set Form ID.
   *
   * @return string
   *   Description of the return value, which is a string.
   */
  public function getFormId() {
    return 'commerce_user_points_admin_settings';
  }

  /**
   * {@inheritdoc}
   *
   * Get Editable Config Names.
   *
   * @return string
   *   Description of the return value, which is a string.
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_user_points.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Build Config Form.
   *
   * @param array $form
   *   FormStateInterface $form_state, Request $request.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface $form_state.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request $request.
   *
   * @return array|Object
   *   Description of the return value, which is a array|object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('commerce_user_points.settings');

    // User registration points.
    $form['user_register_points'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User registration points'),
      '#attributes' => [
        'type' => 'number',
        'class' => ['user-register-points'],
      ],
      '#default_value' => $config->get('user_register_points'),
    ];
    $form['threshold_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Threshold amount'),
      '#attributes' => [
        'type' => 'number',
        'class' => ['user-register-points'],
      ],
      '#default_value' => $config->get('threshold_value'),
    ];

    // Default Percentage.
    $form['order_point_discount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Percentage'),
      '#attributes' => [
        'type' => 'number',
      ],
      '#default_value' => $config->get('order_point_discount'),
    ];

    $form['date_discount'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#description' => $this->t('Select day on which specific discount is applicable.'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['advance-settings'],
      ],
    ];
    // @todo make field dynamic
    $options = [
      '1' => $this->t('Monday'),
      '2' => $this->t('Tuesday'),
      '3' => $this->t('Wednesday'),
      '4' => $this->t('Thurday'),
      '5' => $this->t('Friday'),
      '6' => $this->t('Saturday'),
      '7' => $this->t('Sunday'),
    ];

    $form['date_discount']['day_point_discount'] = [
      '#type' => 'select',
      '#title' => $this->t('Day'),
      '#options' => $options,
      '#description' => $this->t('Add points to select'),
      '#default_value' => $config->get('day_point_discount'),
    ];

    $form['date_discount']['date_point_discount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Day Percentage'),
      '#attributes' => [
        'type' => 'number',
      ],
      '#default_value' => $config->get('date_point_discount'),
    ];

    $form['role_configuration'] = [
      '#type' => 'checkboxes',
      '#options' => ['role' => 'Set to role configuration'],
      '#attributes' => ['class' => ['role-configuration-user-point']],
      '#default_value' => $config->get('role_configuration') ?? [],
    ];
    $roles = $this->roleManager->loadMultiple() ?? [];
    foreach ($roles as $value) {
      $rows[] = $value->id();

      $form['contactform'] = [
        '#type' => 'vertical_tabs',
      ];

      $form['info_' . $value->id()] = [
        '#type' => 'details',
        '#title' => $value->get('label'),
        '#group' => 'contactform',
      ];

      $form['info_' . $value->id()]['user_register_points_' . $value->id()] = [
        '#type' => 'textfield',
        '#title' => $this->t('User registration points'),
        '#attributes' => [
          'type' => 'number',
          'class' => ['user-register-points-role'],
        ],
        '#default_value' => $config->get('user_register_points_' . $value->id()),
      ];
      $form['info_' . $value->id()]['threshold_value_' . $value->id()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Threshold amount'),
        '#attributes' => [
          'type' => 'number',
          'class' => ['threshold-points-role'],
        ],
        '#default_value' => $config->get('threshold_value_' . $value->id()),
      ];

      $form['info_' . $value->id()]['order_point_discount_' . $value->id()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Percentage'),
        '#attributes' => [
          'type' => 'number',
          'class' => ['order-point-role'],
        ],
        '#default_value' => $config->get('order_point_discount_' . $value->id()),
      ];

      $form['info_' . $value->id()]['date_discount_' . $value->id()] = [
        '#type' => 'details',
        '#title' => $this->t('Advanced settings'),
        '#description' => $this->t('Select day on which specific discount is applicable.'),
        '#open' => TRUE,
        '#attributes' => [
          'class' => ['advance-settings-role'],
        ],
      ];

      // @todo make field dynamic
      $options = [
        '1' => $this->t('Monday'),
        '2' => $this->t('Tuesday'),
        '3' => $this->t('Wednesday'),
        '4' => $this->t('Thurday'),
        '5' => $this->t('Friday'),
        '6' => $this->t('Saturday'),
        '7' => $this->t('Sunday'),
      ];

      $form['info_' . $value->id()]['date_discount_' . $value->id()]['day_point_discount_' . $value->id()] = [
        '#type' => 'select',
        '#title' => $this->t('Day'),
        '#options' => $options,
        '#description' => $this->t('Add points to select'),
        '#default_value' => $config->get('day_point_discount_' . $value->id()),
        '#attributes' => [
          'class' => ['date-discount-role'],
        ],
      ];

      $form['info_' . $value->id()]['date_discount_' . $value->id()]['date_point_discount_' . $value->id()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Day Percentage'),
        '#attributes' => [
          'type' => 'number',
          'class' => ['date-point-role'],
        ],
        '#default_value' => $config->get('date_point_discount_' . $value->id()),
      ];
    }
    $form['#attached']['library'][] = 'commerce_user_points/referral_link_library';
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Save Config Form Data.
   *
   * @param array $form
   *   FormStateInterface $form_state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface $form_state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $roles = $this->roleManager->loadMultiple() ?? [];

    foreach ($roles as $value) {
      $this->config('commerce_user_points.settings')
        ->set('user_register_points', $values['user_register_points'])
        ->set('order_point_discount', $values['order_point_discount'])
        ->set('role_configuration', $values['role_configuration'])
        ->set('day_point_discount', $values['day_point_discount'])
        ->set('date_point_discount', $values['date_point_discount'])
        ->set('threshold_value', $values['threshold_value'])
        ->set('user_register_points_' . $value->id(), $values['user_register_points_' . $value->id()])
        ->set('order_point_discount_' . $value->id(), $values['order_point_discount_' . $value->id()])
        ->set('day_point_discount_' . $value->id(), $values['day_point_discount_' . $value->id()])
        ->set('date_point_discount_' . $value->id(), $values['date_point_discount_' . $value->id()])
        ->set('threshold_value_' . $value->id(), $values['threshold_value_' . $value->id()])
        ->save();
    }
  }

}
