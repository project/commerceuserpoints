<?php

namespace Drupal\commerce_user_points\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures referral points settings.
 */
class ReferralPointsConfigurationForm extends ConfigFormBase {

  /**
   * The role manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleManager;

  /**
   * Constructs a new YourConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->roleManager = $entity_type_manager->getStorage('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Set Form ID.
   *
   * @return string
   *   Description of the return value, which is a string.
   */
  public function getFormId() {
    return 'referral_points_admin_settings';
  }

  /**
   * {@inheritdoc}
   *
   * Get Editable Config Names.
   *
   * @return string
   *   Description of the return value, which is a string.
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_user_points.referral_settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Build Config Form.
   *
   * @param array $form
   *   FormStateInterface $form_state, Request $request.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface $form_state.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request $request.
   *
   * @return array|Object
   *   Description of the return value, which is a array|object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('commerce_user_points.referral_settings');

    // Sender Referral Points.
    $form['sender_referral_points'] = [
      '#type' => 'number',
      '#title' => $this->t('Sender referral points'),
      '#description' => $this->t('Points for the user who sent the link and invited the new user.'),
      '#attributes' => [
        'type' => 'number',
        'min' => '0',
      ],
      '#default_value' => $config->get('sender_referral_points'),
    ];

    // New User Referral Points.
    $form['new_user_referral_points'] = [
      '#id' => 'new-user-referral-points',
      '#type' => 'number',
      '#title' => $this->t('New User referral points'),
      '#description' => $this->t('Points get by new user after join by link.'),
      '#attributes' => [
        'type' => 'number',
        'min' => '0',
      ],
      '#default_value' => $config->get('new_user_referral_points'),
    ];

    $form['role_configuration'] = [
      '#type' => 'checkboxes',
      '#options' => ['role' => 'Set to role configuration'],
      '#attributes' => ['class' => ['role-configuration']],
    ];
    $roles = $this->roleManager->loadMultiple();
    foreach ($roles as $value) {

      $rows[] = $value->id();

      $form['contactform'] = [
        '#type' => 'vertical_tabs',
      ];

      $form['info_' . $value->id()] = [
        '#type' => 'details',
        '#title' => $value->get('label'),
        '#group' => 'contactform',
      ];

      $form['info_' . $value->id()]['sender_referral_points_' . $value->id()] = [
        '#type' => 'number',
        '#title' => $this->t('Sender referral points'),
        '#description' => $this->t('Points for the user who sent the link and invited the new user.'),
        '#attributes' => [
          'type' => 'number',
          'min' => '0',
          'class' => ['sender-referral-role'],
        ],
        '#default_value' => $config->get('sender_referral_points_' . $value->id()),
      ];
      $form['info_' . $value->id()]['new_user_referral_points_' . $value->id()] = [
        '#id' => 'new-user-referral-points',
        '#type' => 'number',
        '#title' => $this->t('New User referral points'),
        '#description' => $this->t('Points get by new user after join by link.'),
        '#attributes' => [
          'type' => 'number',
          'min' => '0',
          'class' => ['new-user-referral-role'],
        ],
        '#default_value' => $config->get('new_user_referral_points_' . $value->id()),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Save Config Form Data.
   *
   * @param array $form
   *   FormStateInterface $form_state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface $form_state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $roles = $this->roleManager->loadMultiple();

    foreach ($roles as $value) {

      $this->config('commerce_user_points.referral_settings')
        ->set('new_user_referral_points', $values['new_user_referral_points'])
        ->set('sender_referral_points', $values['sender_referral_points'])
        ->set('sender_referral_points_' . $value->id(), $values['sender_referral_points_' . $value->id()])
        ->set('new_user_referral_points_' . $value->id(), $values['new_user_referral_points_' . $value->id()])
        ->save();
    }

  }

}
