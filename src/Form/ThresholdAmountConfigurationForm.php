<?php

namespace Drupal\commerce_user_points\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures referral points settings.
 */
class ThresholdAmountConfigurationForm extends ConfigFormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The role manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleManager;

  /**
   * Constructs a new YourConfigForm object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $current_user;
    $this->roleManager = $entity_type_manager->getStorage('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Set Form ID.
   *
   * @return string
   *   Description of the return value, which is a string.
   */
  public function getFormId() {
    return 'threshold_amount_admin_settings';
  }

  /**
   * {@inheritdoc}
   *
   * Get Editable Config Names.
   *
   * @return string
   *   Description of the return value, which is a string.
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_user_threshold_amount.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Build Config Form.
   *
   * @param array $form
   *   FormStateInterface $form_state, Request $request.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface $form_state.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request $request.
   *
   * @return array|Object
   *   Description of the return value, which is a array|object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('commerce_user_threshold_amount.settings');

    // Threshold Amount.
    $form['user_threshold_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold Amount'),
      '#description' => $this->t('User can used threshold amount on order.'),
      '#attributes' => [
        'type' => 'number',
        'min' => '0',
      ],
      '#default_value' => $config->get('user_threshold_amount'),
    ];

    $roles = $this->roleManager->loadMultiple();

    foreach ($roles as $value) {
      $form['role_configuration_threshold'] = [
        '#type' => 'checkboxes',
        '#options' => ['role' => 'Set to role configuration'],
        '#attributes' => ['class' => ['role-configuration-threshold']],
      ];
    }

    foreach ($roles as $value) {
      $rows[] = $value->id();
      $form['contactform'] = [
        '#type' => 'vertical_tabs',
      ];

      $form['info_' . $value->id()] = [
        '#type' => 'details',
        '#title' => $value->get('label'),
        '#group' => 'contactform',
      ];

      $form['info_' . $value->id()]['user_threshold_amount_' . $value->id()] = [
        '#type' => 'number',
        '#title' => $this->t('Threshold Amount'),
        '#description' => $this->t('User can used threshold amount on order.'),
        '#attributes' => [
          'type' => 'number',
          'min' => '0',
          'class' => ['role-threshold-field'],
        ],
        '#default_value' => $config->get('user_threshold_amount_' . $value->id()),
      ];

      $role = $this->currentUser->getRoles();
      $form['#attached']['library'][] = 'commerce_user_points/referral_link_library';
      $form['#attached']['drupalSettings']['commerceuserpoints']['role'] = $role;

    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Save Config Form Data.
   *
   * @param array $form
   *   FormStateInterface $form_state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface $form_state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $roles = $this->roleManager->loadMultiple();

    foreach ($roles as $value) {
      $this->config('commerce_user_threshold_amount.settings')
        ->set('user_threshold_amount', $values['user_threshold_amount'])
        ->set('user_threshold_amount_' . $value->id(), $values['user_threshold_amount_' . $value->id()])
        ->save();
    }

    $this->config('commerce_user_threshold_amount.settings')
      ->set('user_threshold_amount', $values['user_threshold_amount'])
      ->save();
  }

}
