<?php

namespace Drupal\commerce_user_points;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;

/**
 * Get Referral link.
 *
 * @package Drupal\referrallink
 * Class ReferralLinkService.
 */
class ReferralLinkService {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The time service.
   *
   * @var \Drupal\Core\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Connection object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time object.
   */
  public function __construct(Connection $database, TimeInterface $time) {
    $this->database = $database;
    $this->time = $time;
  }

  /**
   * Get valid Referral link.
   */
  public function getLink($field, $value = NULL) {
    $query = $this->database->select('user_points_referral', 'upr')
      ->fields('upr', ['referral_link_code'])
      ->condition('upr.' . $field . '', $value);
    $result = $query->execute()->fetchAssoc();
    return $result;
  }

  /**
   * Get valid Referral link.
   */
  public function getLinkUser($value = NULL) {
    $query = $this->database->select('user_points_referral', 'upr')
      ->fields('upr', ['uid'])
      ->condition('upr.referral_link_code', $value);
    $result = $query->execute()->fetchAssoc();
    return $result;
  }

  /**
   * Get add Referral link.
   */
  public function addLink($uid = NULL) {
    $link = "";
    $chars = "0123456789abcdefghijklmnopqrstuvwxyz";
    for ($i = 0; $i < 20; $i++) {
      $link .= $chars[mt_rand(0, strlen($chars) - 1)];
    }

    // Inesert in database.
    $this->database->insert('user_points_referral')->fields([
      'uid' => $uid,
      'referral_link_code' => $link,
      'timestamp' => $this->time->getRequestTime(),
      'status' => 'valid',
    ])->execute();
  }

}
