<?php

namespace Drupal\commerce_user_points\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides methods for ordercomplete..
 *
 * @package Drupal\commerce_user_points
 * Class OrderCompleteSubscriber.
 */
class OrderCompleteSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   About this.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   */
  public function __construct(EntityTypeManager $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   *
   * Get Subscribed Events.
   *
   * @return events
   *   Description of the return value, which is a events.
   */
  public static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['orderCompleteHandler'];
    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.
   *
   *   Post_transition.
   *
   *   Event is dispatched.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Event.
   */
  public function orderCompleteHandler(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $data = $order->getData('user_points_to_deduct');

    if (!empty($data['nids']) && !empty($data['points'])) {
      $this->deductUserPoints($data['nids'], $data['points']);
    }

    $orderUid = $order->getCustomerId();
    $orderEmail = $order->getEmail();
    $orderSubtotal = $order->getSubtotalPrice();
    // @todo Uncomment below if needed in future.
    // $orderTotal = $order->getTotalPrice();
    $totalOrderAmount = $orderSubtotal->getNumber();
    $orderPointDiscount = $this->configFactory->get('commerce_user_points.settings')->get('order_point_discount');
    $dayPointDiscount = $this->configFactory->get('commerce_user_points.settings')->get('day_point_discount');
    $datePointDiscount = $this->configFactory->get('commerce_user_points.settings')->get('date_point_discount');

    // Get discount percentage.
    $orderAppliedDiscount = $orderPointDiscount;

    // Update discount percentage if day is same.
    if (gmdate('N') == $dayPointDiscount) {
      $orderAppliedDiscount = $datePointDiscount;
    }

    // Built array to save "user_point" node.
    $arrNode = [
      'type' => 'user_points',
      'title' => "New order " . $order->getOrderNumber() . " for " . $orderEmail . ", UID: " . $orderUid,
      'uid' => $orderUid,
      'field_earned_points' => round(($totalOrderAmount * $orderAppliedDiscount) / 100),
      'field_points_acquisition_date' => gmdate('Y-m-d'),
      'field_point_status' => 1,
      'field_point_type' => 'shopping',
      'field_used_points' => 0,
      'field_validity_date' => gmdate('Y-m-d', strtotime('+1 years')),
    ];

    // Save new node.
    $nodeEntity = Node::create($arrNode);
    $nodeEntity->save();
  }


  /**
   * Deduct user points from nodes.
   *
   * @param array $userPointsNids
   *   The node IDs.
   * @param int $totalUsablePoints
   *   The total points to deduct.
   *
   * @return bool
   *   TRUE if points deducted successfully.
   */
  protected function deductUserPoints(array $userPointsNids, $totalUsablePoints) {
    $updatedPoints = 0;
    $calculatedRemainingPoints = $totalUsablePoints;

    // Load nodes and deduct points.
    $nodes = Node::loadMultiple($userPointsNids);

    foreach ($nodes as $node) {
      $earnedNodePoints = $node->get('field_earned_points')->getString();
      $usedNodePoints = $node->get('field_used_points')->getString();
      $availableNodePoints = $earnedNodePoints - $usedNodePoints;

      $nextDeductPoints = $calculatedRemainingPoints - $availableNodePoints;
      if ($updatedPoints < $totalUsablePoints) {
        if ($nextDeductPoints > 0) {
          $updatedPoints += $availableNodePoints;
          $calculatedRemainingPoints = $nextDeductPoints;
          $nodeUpdatePoints = $usedNodePoints + $availableNodePoints;
          $node->set('field_used_points', $nodeUpdatePoints);
          $node->save();
        }
        else {
          $updatedPoints += $calculatedRemainingPoints;
          $nodeUpdatePoints = $usedNodePoints + $calculatedRemainingPoints;
          $node->set('field_used_points', $nodeUpdatePoints);
          $node->save();
        }
      }
    }

    return TRUE;
  }

}
