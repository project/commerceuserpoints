<?php

namespace Drupal\commerce_user_points\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for register user by referral link.
 */
class UserReferralLink extends ControllerBase {

  /**
   * Referral Registration Form.
   */
  public function link() {
    return [];
  }

}
