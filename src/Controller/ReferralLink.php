<?php

namespace Drupal\commerce_user_points\Controller;

use Drupal\commerce_user_points\ReferralLinkService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for register user by referral link.
 */
class ReferralLink extends ControllerBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The referral link service.
   *
   * @var \Drupal\commerce_user_points\ReferralLinkService
   */
  protected $referralLinkService;

  /**
   * Constructs a new YourControllerBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\commerce_user_points\ReferralLinkService $referral_link_service
   *   The referral link service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, RequestStack $request_stack, FormBuilderInterface $form_builder, AccountProxyInterface $current_user, ReferralLinkService $referral_link_service) {
    $this->entityManager = $entity_manager;
    $this->requestStack = $request_stack;
    $this->formBuilder = $form_builder;
    $this->currentUser = $current_user;
    $this->referralLinkService = $referral_link_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('form_builder'),
      $container->get('current_user'),
      $container->get('commerce_user_points.referral_link')
    );
  }

  /**
   * Referral Registration Form.
   */
  public function registration() {

    $uid = $this->currentUser->id();
    // Get current url of referral link page.
    $request = $this->requestStack->getCurrentRequest();
    $current_path = $request->getRequestUri();
    $pathParameters = explode("referral/", $current_path);
    if (isset($pathParameters[1]) && !empty($pathParameters[1])) {
      $link = $this->referralLinkService->getLink('referral_link_code', $pathParameters[1]);

      if ($link != NULL && $uid == NULL) {
        // Render registration form.
        $entity = $this->entityManager
          ->getStorage('user')
          ->create([]);
        $formObject = $this->entityManager
          ->getFormObject('user', 'register')
          ->setEntity($entity);
        return $this->formBuilder->getForm($formObject);
      }
      else {
        $frontPage = Url::fromRoute('<front>')->setAbsolute()->toString();
        $register_page = $frontPage . '/user/register';
        $response = new RedirectResponse($register_page);
        $response->send();
        return;
      }
    }
  }

}
