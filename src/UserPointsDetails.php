<?php

namespace Drupal\commerce_user_points;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;

/**
 * Class UserPointsDetails.
 *
 * Provides details about user points.
 *
 * @package Drupal\commerce_user_points
 */
class UserPointsDetails {

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(AccountProxyInterface $currentUser, EntityTypeManagerInterface $entityTypeManager) {
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Calculate user available points.
   *
   * @return array
   *   An array containing the total usable points and user points node IDs.
   */
  public function calculateUsablePoints() {
    // Get all valid user points.
    $user = $this->currentUser;

    $bundle = 'user_points';
    $storage = $this->entityTypeManager->getStorage('node');
    $query = $storage->getQuery();
    $query->condition('status', 1);
    $query->condition('type', $bundle);
    $query->condition('uid', $user->id());

    $validityCondition = $query->orConditionGroup();
    $validityCondition->notExists('field_validity_date.value');
    $validityCondition->condition('field_validity_date.value', gmdate('Y-m-d'), '>=');
    $query->condition($validityCondition);

    $query->condition('field_point_status.value', '1', '=');
    $query->sort('field_validity_date.value', 'ASC');
    $query->accessCheck(TRUE);
    $entityIds = $query->execute();

    $nodes = $storage->loadMultiple($entityIds);

    $totalEarnedPoints = 0;
    $totalUsedPoints = 0;

    foreach ($nodes as $nodeObject) {
      $totalEarnedPoints += $nodeObject->get('field_earned_points')->getString();
      $totalUsedPoints += $nodeObject->get('field_used_points')->getString();
    }

    // Total usable points by logged in user.
    $totalUsablePoints = $totalEarnedPoints - $totalUsedPoints;

    $arrNidPoints = [];
    $arrNidPoints['total_usable_points'] = round($totalUsablePoints);
    $arrNidPoints['user_points_nids'] = $entityIds;

    return $arrNidPoints;
  }

}
